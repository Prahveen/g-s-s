const GSR = require('google-search-results-nodejs');
const ARGV = require('yargs').argv;


let client = new GSR.GoogleSearchResults("91c69da2a7797e2271e6869f626ff448989f633906067ac7fd660dd74d11e4cd")


var callback = function(data) {
	if (data.search_metadata.status === "Success")
	{
		console.log(data.organic_results)
	}
}


if (ARGV.kwd !== undefined) {
	var parameter = {
   	q: ARGV.kwd,
    	hl: "sv",
    	gl: "se",
    	google_domain: "google.se",
	};
	client.json(parameter, callback)
}
else {
	console.log('Missing kwd arg, Ex node serpapi --kwd=keywordToSearch')
}


