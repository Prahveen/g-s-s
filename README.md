## Install Dependency
	```
		$ npm install
	```

## Search Keyword
	```
		$ node serapi.js --kwd='Search Keyword'

	```

## Example
    ```
    $  node serapi.js --kwd='nextory'

    [ { position: 1,
        title: 'Nextory',
        link: 'https://www.nextory.se/',
        displayed_link: 'https://www.nextory.se/',
        snippet: 'Prova tusentals Ljudböcker och E-böcker Gratis i 14 dagar! Ladda ner appen och streama i din iPhone, Android eller iPad var du än är med Nextory.',
        sitelinks: { expanded: [Array] },
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:cy5MFRFsAyQJ:https://www.nextory.se/+&cd=1&hl=sv&ct=clnk&gl=se',
        related_pages_link: 'https://www.google.se/search?hl=sv&gl=se&q=related:https://www.nextory.se/+nextory&tbo=1&sa=X&ved=2ahUKEwjwifX48oPjAhWswFkKHbo2AB0QHzAAegQIBhAH' },
      { position: 2,
        title: 'Storytel, Bookbeat eller Nextory – vad passar dig bäst? - Expressen',
        link: 'https://www.expressen.se/dinapengar/konsument/storytel-bookbeat-eller-nextory-har-ar-tjansten-som-ar-bast-for-dig/',
        displayed_link: 'https://www.expressen.se/.../storytel-bookbeat-eller-nextory-har-ar-tjansten-som-ar-bast-...',
        date: '18 sep. 2018',
        snippet: 'Väljer du det billigare alternativet från Nextory får du dock vänta ett halvår innan du kan lyssna på den nya boken alla pratar om i lunchrummet.' },
      { position: 3,
        title: 'Prova Nextory kostnadsfritt i 30 dagar! | fortum.se',
        link: 'https://www.fortum.se/privat/elavtal/kunderbjudanden/nextory',
        displayed_link: 'https://www.fortum.se/privat/elavtal/kunderbjudanden/nextory',
        snippet: 'Nextory är en digital boktjänst som du som är Fortumkund nu får testa kostnadsfritt i 30 dagar! Hållbarhet är en central del av Fortums strategi för att möta dagens ...',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:c_my0PSruxoJ:https://www.fortum.se/privat/elavtal/kunderbjudanden/nextory+&cd=9&hl=sv&ct=clnk&gl=se' },
      { position: 4,
        title: 'Nextory: Ljudböcker & E-böcker – Appar på Google Play',
        link: 'https://play.google.com/store/apps/details?id=com.gtl.nextory&hl=sv',
        displayed_link: 'https://play.google.com/store/apps/details?id=com.gtl.nextory&hl=sv',
        snippet: 'Med Nextory kan du läsa eller lyssna på böcker direkt i din telefon eller surfplatta. Skapa ett konto för att prova Nextory! Funktioner: • Läs dina e-böcker • Lyssna ...',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:6kdFCMywV7AJ:https://play.google.com/store/apps/details%3Fid%3Dcom.gtl.nextory%26hl%3Dsv+&cd=10&hl=sv&ct=clnk&gl=se',
        related_pages_link: 'https://www.google.se/search?hl=sv&gl=se&q=related:https://play.google.com/store/apps/details%3Fid%3Dcom.gtl.nextory%26hl%3Dsv+nextory&tbo=1&sa=X&ved=2ahUKEwjwifX48oPjAhWswFkKHbo2AB0QHzAJegQIAxAF',
        rich_snippet: { top: [Object] } },
      { position: 5,
        title: 'Stort test 2018: Storytel vs BookBeat vs Nextory vs Bokus Play - välj ...',
        link: 'https://www.boktugg.se/2018/09/19/stort-test-2018-storytel-vs-bookbeat-vs-nextory-vs-bokus-play-valj-ljudbokstjanst/',
        displayed_link: 'https://www.boktugg.se/.../stort-test-2018-storytel-vs-bookbeat-vs-nextory-vs-bokus-p...',
        date: '19 sep. 2018',
        snippet: 'Vilken tjänst är bäst för ljudböcker och eböcker i mobilen? Vi har gjort ett stort test av svenska ljudbokstjänsterna Storytel, Bookbeat, Nextory ...',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:MOaQ4KBdsq4J:https://www.boktugg.se/2018/09/19/stort-test-2018-storytel-vs-bookbeat-vs-nextory-vs-bokus-play-valj-ljudbokstjanst/+&cd=11&hl=sv&ct=clnk&gl=se' },
      { position: 6,
        title: 'Läs det här innan du väljer Nextory | Letabok.se',
        link: 'https://letabok.se/nextory/',
        displayed_link: 'https://letabok.se/nextory/',
        snippet: 'Vad kostar det egentligen, hur många enheter och vilket utbud har Nextory egentligen? Vi redovisar allt!',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:9JtLw2bshCkJ:https://letabok.se/nextory/+&cd=12&hl=sv&ct=clnk&gl=se' },
      { position: 7,
        title: 'Nextory – Stort utbud e-böcker & ljudböcker – Prova gratis i 2 veckor!',
        link: 'http://www.xn--jmfrljudbcker-bfb6yg.se/nextory',
        displayed_link: 'www.jämförljudböcker.se/nextory',
        snippet: 'Nextory erbjuder ljudböcker och e-böcker i abonnemangsform. När du har ett abonnemang får du lyssna och läsa obegränsat antal böcker under tiden ditt…',
        cached_page_link: 'http://webcache.googleusercontent.com/search?q=cache:s-UOvYJk6P4J:www.xn--jmfrljudbcker-bfb6yg.se/nextory+&cd=13&hl=sv&ct=clnk&gl=se' } ]
  ```
